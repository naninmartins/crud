<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Http\Requests\ClientRequest;



/*
Organização do pensamento:


Não ser redundante nas views

Estudar tipo de dados no laravel, builder, collection, array, Estudar helpers
Estudar select2 e como o bootstrap funciona
Estudar SOC separation of concerns
Seed Factory e Boas Praticas Best Pratices



*/

class ClientController extends Controller
{
    private $Client;

    function __construct(Client $client)
    {
        $this->Client = $client;
    }

    public function index()
    {
        $clients = $this->Client->all();

        return view('Client/index', compact('clients'));
    }

    public function show(Request $request)
    {
        //* Display the specified resource.
       

        if ($request->option == 1)
        {
            $nome = $request->only('search');
            $clients = $this->Client->where('name',$nome)->get();

        }
        else
        {
       
            $cnpj = $request->get('search');
            $clients = $this->Client->where('cnpj',$cnpj)->get();
    
        }
       
        return view('Client/index', compact('clients'));

   
     
    }

    public function create(Client $client=null)
    {
        //Show the form for creating a new resource.
        return view('Client/create-edit',compact('client'));
    }
   
    public function store(ClientRequest $request)
    {
        //* Store a newly created resource in storage.
        //$request->input('name'); somente no campo definido;

        //cata todos os dados do form
        
        $dataform = $request->except(['_token']);

        //validations forms data     
        //$this->validate ($request, $request->rules, $request->messages)); METODO VALIDATE SOMENTE NECESSÁRIO SE NÃO HOUVER REQUEST

        //faz o cadastro
        $insert = $this->Client->create($dataform);

         /* $insert = $this->Client->create([
            'name' => 'jacu', 'cnpj' => 156481,

        ]); Metodo para inserir manualmente no BD*/

        if ($insert)
           return redirect('client/index')->with('status-store', 'Cliente Cadastrado!');

        else
            return redirect('client/index')->with('erro', 'Erro ao Cadastrar!');
      
    }

    public function edit(Request $request)
    {
         //* Show the form for editing the specified resource.
    }

    public function update(ClientRequest $request)
    {
        //* Update the specified resource in storage.
        $client = $this->Client->find($request->id);
        $update = $client->update($request->all());

        if ($update)
           return redirect('client/index')->with('status-update', 'Cliente Atualizado!');

        else
            return redirect('client/index')->with('erro', 'Erro ao Atualizar!');

    }

    public function destroy(Request $request)
    {
        //* Remove the specified resource from storage.
        
        $client = $this->Client->find($request->id);
        $delete = $client->delete();

        if ($delete)
           return redirect('client/index')->with('status-destroy', 'Cliente Deletado!');

        else
            return redirect('client/index')->with('erro', 'Erro ao Deletar!');

    }

}