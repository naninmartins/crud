<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required|min:3|max:100',
            'price' => 'required',
            'description' => 'required|min:3|max:100',
        ];

    }

    public function messages()
    {
        return [
            'product_name.required' => 'Nome do Produto de preenchimento obrigatório',
            'product_name.min' => 'Nome do Produto com min de 3 caracteres e max de 100',
            'price.required' => 'Preço de preenchimento obrigatório',
            'description.required' => 'Descrição de preenchimento obrigatório',
            'description.min' => 'Descrição com min de 3 caracteres e max de 100',
        ];

    }
}
