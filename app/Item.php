<?php

namespace App;
use App\Product;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //

    protected $fillable = [
        'product_id', 'quantity','order_id','sub_price',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $guarded = [
        '_token',
    ];


    public function subPrice ($dataform)
    {
        
        $product_id = array_get($dataform,'product_id');

        $quantity = array_get($dataform,'quantity');

        $product = Product::find($product_id);

        $sub_price = $product->price * $quantity;

        $dataform = array_add($dataform, 'sub_price', $sub_price);

        return $dataform;

    }



    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
