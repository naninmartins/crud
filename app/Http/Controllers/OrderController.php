<?php

namespace App\Http\Controllers;
use App\Http\Requests\OrderRequest;
use App\Order;
use App\Client;
use App\Product;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $Order;

    function __construct(Order $order)
    {
        $this->Order = $order;
    }

    public function index()
    {
        $orders = $this->Order->all();
        $clients = Client::all()->pluck('name','id');  
        
        /*$order = $this->Order->with('client')->find(1);
        dd($order->client->name);*/
        return view('Order/order', compact('orders','clients'));
    }

    public function show($id)
    {
        $products = Product::all()->pluck('product_name','id');
    
        $order = $this->Order->find($id);

        $items = $order->items->all();

        $sub_prices = array_pluck($items,'sub_price');

        $price = collect($sub_prices)->sum();

         return view('Item/order-detail', compact('order','items','price','products'));
    }

    public function search(Request $request)
    {
        $clients = Client::all()->pluck('name','id');
        if ($request->option == 1)
        {
            $search = $request->only('search');
            $orders = $this->Order->where('salesman',$search)->get();
            //$order->client->name
        }
        else
        {
       
            $search = $request->get('search');
            $clients = Client::all();
            $clients = Client::where('name',$search)->get();
            $search = array_pluck($clients,'id');
            $orders = $this->Order->where('client_id',$search)->get();
    
        }
       
        return view('Order/order', compact('orders', 'clients'));


    }

    public function create(Order $order=null)
    {
        //Show the form for creating a new resource.

         $clients = Client::all()->pluck('name','id');
        

        return view('Order/create_edit',compact('order','clients'));
    }
    
    public function store(OrderRequest $request)
    {
     
        $dataform = $request->except(['_token']);

        $insert = $this->Order->create($dataform);


        if ($insert)
           return redirect('order')->with('status-store', 'Pedido Cadastrado!');
           
        else
            return redirect('order')->with('erro', 'Erro ao Cadastrar!');
            
      
    }

    public function update(OrderRequest $request)
    {
        //* Update the specified resource in storage.
        $order = $this->Order->find($request->id);
        $update = $order->update($request->all());

        if ($update)
           return redirect('order')->with('status-update', 'Pedido Atualizado!');
            
        else
            return redirect('order')->with('erro', 'Erro ao Atualizar!');
            
    }
    public function destroy(Request $request)
    {
        //* Remove the specified resource from storage.
        
        $order = $this->Order->find($request->id);
        $delete = $order->delete();

        

        if ($delete)
           return redirect('order')->with('status-destroy', 'Pedido Deletado!');

        else
            return redirect('order')->with('erro', 'Erro ao Deletar!');

    }
}