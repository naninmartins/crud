@extends('admin.dashboard')
@include('admin.message')
@section('content')


@if(isset($order)) 
{!! Form::model($order, ['url'=>'order/update/{$order->id}', 'class' => 'form', 'method' =>'PUT'])!!} 
@else
{!! Form::open(['url'=>'order/store', 'id'=>'formulario']) !!} @endif



{!! Form::open(['url' => ['order/update'],'method' => 'put']) !!}


{!! Form::hidden('id', null, ['id'=>'id']) !!}
{!! Form::label('lsalesman', 'Vendedor') !!}        
{!! Form::text('salesman', null, ['class' => 'form-control input-lg', 'placeholder'=>'Vendedor', 'id'=>'salesman']) !!}
{!! Form::label('lclient', 'Cliente') !!} 
{!! Form::select('client_id', $clients, null, ['class' => 'form-control input-lg']) !!}
{!! Form::label('ldate_order', 'Data do Pedido') !!} 
{!! Form::date('date_order', null, ['class' => 'form-control input-lg', 'placeholder'=>'Data do Pedido', 'id'=>'date_order']) !!}               
{!! Form::submit('Salvar',['class' => 'btn btn-primary']) !!}
<button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Cancelar</button>
{!! Form::close() !!}
@endsection()




