@extends('admin.dashboard')
@section('content')
@include('admin.message')

<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Visualizar Dados do Pedido</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-minus"></i></button>
            </div>

        </div>

        <div class="box-body" style="">
                
            <div class="row">

                <div class="col-md-3">
                    <b>Numero do Pedido:</b> {{$order->id}}
                </div>

                <div class="col-md-4">
                        <b>Data do Pedido:</b> {{ \Carbon\Carbon::parse($order->date_order)->format('d/m/Y') }} 
                </div>

                <div class="col-md-4">
                        <b>Valor do Pedido:</b> {{$price}} 
                </div>
                
            </div>

            <div class="row">

                <div class="col-md-3">
                        <b> Vendedor:</b> {{$order->salesman}}
                </div>

                <div class="col-md-3">
                    <b> Cliente:</b> {{$order->client->name}}
                </div>

                <div class="col-md-5 pull-right">
                        <b> Adicionar Item: </b>
                        <button  type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#CreateModal" 
                            data-title="Cadastrar Pedido"
                            data-order_id="{{$order->id}}">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>

            </div>
                    
        </div>

        <!-- /.box-body -->
        <div class="box" style="">

            <div>     
                    <table id="example2" class="table table-bordered table-hover dataTable" >
                        <thead>
                            
                            <tr role="row">
                                <th>
                                    Descrição dos Itens
                                </th>
                            <tr role="row">
                                <th>
                                    Quantidade
                                </th>
        
                                <th >
                                        Produto
                                    </th>
        
                                <th >
                                    Valor Unitário
                                </th>
        
                                <th >
                                        Valor Total
                                </th>

                                <th >
                                    Ações
                            </th>
        
                            </tr>
                        </thead>
        
                        <tbody>
                                @foreach($items as $item)
                                <tr role="row" class="odd">    
                                    <td>{{$item->quantity}}</td> 
                                    <td>{{$item->product->product_name}}</td>
                                    <td>{{$item->product->price}}</td>                               
                                    <td>{{$item->sub_price}}</td>                                            
                                    <td>                                                             
                                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#EditModal" 
                                        data-title="Editar Item" 
                                        data-id="{{$item->id}}"
                                        data-quantity="{{$item->quantity}}"
                                        data-order_id="{{$order->id}}">                              
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#DeleteModal" 
                                        data-id="{{$item->id}}">                                                                    
                                        <i class="glyphicon glyphicon-erase"></i>
                                    </button>
                                    </td>
                                        
                                </tr>
                            @endforeach

                        </tbody>
        
                        <tfoot>
                            
                        </tfoot>
                    </table>    
            </div>

        </div>
        <!-- /.box-footer-->
    </div>

@stop

@extends('admin.form')

    @section('edit_form')               
    {!! Form::open(['url' => ['item/update'],'method' => 'put']) !!}
    {!! Form::hidden('id', null, ['id'=>'id']) !!}
    {!! Form::hidden('order_id', null, ['id'=>'order_id']) !!}
    <div class="row"> 
        <div class="col-md-5"> 
            {!! Form::label('lproduct', 'Produto') !!} 
        </div>
        <div class="col-md-6">
            {!! Form::label('lquantity', 'Quantidade') !!}         
        </div>
    </div> 
    <div class="row"> 
        <span class="form-control input-lg">
            <div class="col-md-5"> 
            {!! Form::select('product_id', $products, null, ['class' => 'js-example-basic-single']) !!}
            </div> 
            <div class="col-md-2">
                {!! Form::number('quantity', 'number', [ 'min' => '1', 'id'=>'quantity']) !!}         
            </div>   
        </span>       
    </div>  
    @endsection

    @section('create_form')
    {!! Form::open(['url' => ['item/store']]) !!}
    {!! Form::hidden('id', null, ['id'=>'id']) !!}
    {!! Form::hidden('order_id', null, ['id'=>'order_id']) !!}
    <div class="row"> 
        <div class="col-md-5"> 
            {!! Form::label('lproduct', 'Produto') !!} 
        </div>
        <div class="col-md-6">
            {!! Form::label('lquantity', 'Quantidade') !!}         
        </div>
    </div> 
    <div class="row"> 
        <span class="form-control input-lg">
            <div class="col-md-5"> 
            {!! Form::select('product_id', $products, null, ['class' => 'js-example-basic-single']) !!}
            </div> 
            <div class="col-md-2">
                {!! Form::number('quantity', 'number', [ 'min' => '1', 'id'=>'quantity']) !!}         
            </div>   
        </span>       
    </div>     
    @endsection

    @section('delete_form')
    {!! Form::open(['url' => ['item/destroy']]) !!}
    {!! Form::hidden('id', null, ['id'=>'id']) !!}
    @endsection
     
    @section('js')
    <script>
        $('#EditModal').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget) // Button that triggered the modal            
            
            $(this).find('#quantity').val(button.data('quantity'))
            $(this).find('#order_id').val(button.data('order_id'))
            $(this).find('#id').val(button.data('id'))
            $(this).find('#order_id').val(button.data('order_id'))
        }); 
        $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });

        $('#CreateModal').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget) // Button that triggered the modal            
    
            $(this).find('#order_id').val(button.data('order_id'))
        }); 

        $('#DeleteModal').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget) // Button that triggered the modal            
            
            $(this).find('#id').val(button.data('id'))
        }); 
    </script>
    @endsection()

