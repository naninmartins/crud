<!-- Edit Modal -->

<div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Editar</h4>
          </div>
          <div class="modal-body">
                @include('admin.message') 

                @yield('edit_form')
                                
          </div>
          <div class="modal-footer">
                <div class="row" style="margin-left:1px;">
                        {!! Form::submit('Salvar',['class' => 'btn btn-primary']) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    </div>
                {!! Form::close() !!}
          </div>
    </div>
  </div>
</div>

<!-- Create Modal -->

<div class="modal fade" id="CreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Criar</h4>
            </div>
            <div class="modal-body">
                  @include('admin.message') 

                  @yield('create_form')
                                    
            </div>
            <div class="modal-footer">
                  <div class="row" style="margin-left:1px;">
                          {!! Form::submit('Salvar',['class' => 'btn btn-primary']) !!}
                          <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Cancelar</button>
                      </div>
                  {!! Form::close() !!}
            </div>
      </div>
    </div>
  </div>

<!-- Delete Modal -->

<div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">ESTE DADO SERÁ DELETADO!!!</h4>
                    </div>
                    <div class="modal-body">
                          @include('admin.message')
                          
                              <h4>Tem certeza que deseja deletá-lo?
        
                          @yield('delete_form')
                                            
                    </div>
                    <div class="modal-footer">
                          <div class="row" style="margin-left:1px;">
                                  {!! Form::submit('Deletar',['class' => 'btn btn-primary']) !!}
                                  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Cancelar</button>
                              </div>
                          {!! Form::close() !!}
                    </div>
              </div>
            </div>
          </div>
        


