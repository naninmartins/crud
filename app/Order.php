<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Client;

class Order extends Model
{
    //
    protected $fillable = [
        'salesman', 'total_price','date_order','client_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $guarded = [
        '_token',
    ];


    public function items()
    {
        return $this->hasMany('App\Item');
    }


    public function client()
    {
        return $this->belongsTo(Client::class,'client_id');
    }


}
