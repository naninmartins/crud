@extends('admin.dashboard')
@section('content')
@include('admin.message')

<div>       
    <table class="table table-bordered table-hover dataTable" >
        <thead>
            <tr class="form-inline" >
                <div class="col-md-3 pull-right"style="padding-rigth:0px;">
                    
                    <button  type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#CreateModal" 
                        data-title="Cadastrar Cliente">
                        <i class="fa fa-plus"></i>
                    </button>     
                        
                    </div>  
                </div>                     
                    <div class="col-md-2 pull-left" style="padding-left:0px;">
                    <div class="input-group"> 
                        <input class="form-control" id="myInput" type="text" placeholder="Pesquisar..">                               
                    </div> 
                </div>  
            </tr>
            <tr role="row">
                <th >
                    Cliente
                </th>

                <th  >
                    CNPJ
                </th>

                <th  >
                    Data de Cadastro
                </th>
                
                <th  >
                    Ações                        
                </th>
            </tr>
        </thead>

        <tbody id="myTable">
            @foreach($clients as $client)
                <tr role="row" class="odd">
                    <td class="sorting_1">{{strtoupper($client->name)}}</td>
                    <td>{{$client->cnpj}}</td>
                    <td>{{ \Carbon\Carbon::parse($client->date_cad)->format('d/m/Y')}}</td>
                    <td>     
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#EditModal" 
                        data-title="Editar Cliente" 
                        data-id="{{$client->id}}"
                        data-name="{{strtoupper($client->name)}}" 
                        data-date_cad="{{$client->date_cad}}"
                        data-cnpj="{{$client->cnpj}}">                              
                        <i class="fa fa-edit"></i>
                    </button>
                    <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#DeleteModal" 
                        data-id="{{$client->id}}">                                                                    
                        <i class="glyphicon glyphicon-erase"></i>
                    </button>
                    </td>                            
                </tr>
            @endforeach
        </tbody>

        <tfoot>
        </tfoot>
    </table>    
</div>

@stop

@extends('admin.form')


    @section('edit_form')               
    {!! Form::open(['url' => ['client/update'],'method' => 'put']) !!}
    {!! Form::hidden('id', null, ['id'=>'id']) !!}    
    {!! Form::label('lname', 'Nome') !!}        
    {!! Form::text('name', null, ['class' => 'form-control input-lg', 'placeholder'=>'Nome','id'=>'name']) !!}
    {!! Form::label('lcnpj', 'CNPJ') !!}
    {!! Form::text('cnpj', null, ['class' => 'form-control input-lg', 'placeholder'=>'CNPJ','maxlength'=>'13','id'=>'cnpj']) !!}              
    {!! Form::date('date_cad', null, ['class' => 'form-control input-lg', 'placeholder'=>'Data de Cadastro','id'=>'date']) !!}                  
    @endsection


    @section('create_form')     
    {!! Form::open(['url' => 'client/store']) !!} 
    {!! Form::hidden('id', null, ['id'=>'id']) !!}    
    {!! Form::label('lname', 'Nome') !!}        
    {!! Form::text('name', null, ['class' => 'form-control input-lg', 'placeholder'=>'Nome','id'=>'name']) !!}
    {!! Form::label('lcnpj', 'CNPJ') !!}
    {!! Form::text('cnpj', null, ['class' => 'form-control input-lg', 'placeholder'=>'CNPJ','maxlength'=>'13','id'=>'cnpj']) !!}              
    {!! Form::date('date_cad', null, ['class' => 'form-control input-lg', 'placeholder'=>'Data de Cadastro','id'=>'date']) !!}          
    
    @endsection

    @section('delete_form')
    {!! Form::open(['url' => ['client/destroy']]) !!}
    {!! Form::hidden('id', null, ['id'=>'id']) !!}
    @endsection
      
    @section('js')
    <script>
        $('#EditModal').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget) // Button that triggered the modal            
            
            $(this).find('#name').val(button.data('name'))
            $(this).find('#cnpj').val(button.data('cnpj'))
            $(this).find('#date').val(button.data('date_cad'))
            $(this).find('#id').val(button.data('id'))
        }); 

        $('#DeleteModal').on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget) // Button that triggered the modal            
        
        $(this).find('#id').val(button.data('id'))
        });

        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
              var value = $(this).val().toLowerCase();
              $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
            });
          });
        
      </script>
    @endsection()






