<?php

namespace App\Http\Controllers;
use App\Item;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;

class ItemController extends Controller
{
    private $Item;

    function __construct(Item $item)
    {
        $this->Item = $item;
    }

    public function index()
    {
        $items = $this->Item->all();
        

        return view('Item/order-detail', compact('items','products'));
    }

    public function show($id)
    {

        dd($item = $this->Item->find($id));
             
        return view('Item/order-detail', compact('item'));

    }

    public function create(Item $item=null)
    {
        
    }
    
    public function store(ItemRequest $request)
    {    
        
        $insert = $this->Item->create($this->Item->subPrice($request->all()));

        if ($insert)
            return redirect()->back()->with('status-store', 'Item Criado!');
           
        else
            return redirect()->back()->with('erro', 'Erro ao Cadastrar!');
            
      
    }

    public function update(ItemRequest $request)
    {
        
        $item = $this->Item->find($request->id);
        $update = $item->update($this->Item->subPrice($request->all()));

        if ($update)
            return redirect()->back()->with('status-update', 'Item Atualizado!');
            
        else
            return redirect()->back()->with('erro', 'Erro ao Atualizar!');
            
    }
    public function destroy(Request $request)
    {
        
        $item = $this->Item->find($request->id);
        $delete = $item->delete();

        if ($delete)
           return redirect()->back()->with('status-destroy', 'Item Deletado!');

        else
        return redirect()->back()->with('erro', 'Erro ao Deletar!');

    }
}

    {
        /*
        $dataform = $request->except(['_token']);

        $order_id = array_get($dataform,'order_id');
        
        $product_id = array_get($dataform,'product_id');

        $quantity = array_get($dataform,'quantity');

        $product = Product::find($product_id);

        $sub_price = $product->price * $quantity;

        $dataform = array_add($dataform, 'sub_price', $sub_price);
        */
    }