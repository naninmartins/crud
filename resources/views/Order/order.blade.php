@extends('admin.dashboard')
@section('content')
@include('admin.message')

<div>     
    <table id="example2" class="table table-bordered table-hover dataTable" >
        <thead>
            <tr class="form-inline" >
                <div class="col-md-3 pull-right"style="padding-left:0px;">                    
                    <button  type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#CreateModal" 
                        data-title="Cadastrar Pedido">
                        <i class="fa fa-plus"></i>
                    </button>
                    
                    <a href="{{url("/order/create")}}" class="btn btn-primary btn-lg" style="margin:2px;"><i class="fa fa-plus"></i></a>                                                                           
                
                </div>  
                </div>                     
                    <div class="col-md-3 pull-left" style="padding:0px;">
                    <div class="input-group"> 
                        <input class="form-control" id="myInput" type="text" placeholder="Pesquisar..">                               
                    </div> 
                </div>  
            </tr>       
            <tr role="row">               
                <th>Numero do Pedido</th>
                <th>Vendedor</th>
                <th>Cliente</th>
                <th>Valor</th>                
                <th>Data do Pedido</th>
                <th>Ações</th>        
            </tr>
        </thead>
        <tbody id="myTable">
            @foreach($orders as $order)
                <tr role="row" class="odd">                                                            
                    <td class="sorting_1">{{strtoupper($order->id)}}</td>
                    <td>{{strtoupper($order->salesman)}}</td> 
                    <td>{{strtoupper($order->client->name)}}</td>                               
                    <td>{{$order->total_price}}</td>
                    <td>{{ \Carbon\Carbon::parse($order->date_order)->format('d/m/Y')}}</td>
                    <td> 
                    <a href="{{url("/order/show/{$order->id}")}}" class="btn btn-info btn-lg" style="margin:2px;"><i class="fa fa-fw fa-eye"></i></a>                                   
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#EditModal" 
                        data-title="Editar Cliente" 
                        data-id="{{$order->id}}"
                        data-salesman="{{strtoupper($order->salesman)}}" 
                        data-client="{{$order->client->name}}"
                        data-date_order="{{$order->date_order}}">                              
                        <i class="fa fa-edit"></i>
                    </button>
                    <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#DeleteModal" 
                        data-id="{{$order->id}}">                                                                    
                        <i class="glyphicon glyphicon-erase"></i>
                    </button>
                    </td>                        
                </tr>
            @endforeach
        </tbody>
        <tfoot></tfoot>
    </table>    
</div>
@stop

@extends('admin.form')
    @section('edit_form')               
    {!! Form::open(['url' => ['order/update'],'method' => 'put']) !!}
    {!! Form::hidden('id', null, ['id'=>'id']) !!}
    {!! Form::label('lsalesman', 'Vendedor') !!}        
    {!! Form::text('salesman', null, ['class' => 'form-control input-lg', 'placeholder'=>'Vendedor', 'id'=>'salesman']) !!}
    {!! Form::label('lclient', 'Cliente') !!} 
    {!! Form::select('client_id', $clients, null, ['class' => 'form-control input-lg']) !!}
    {!! Form::label('ldate_order', 'Data do Pedido') !!} 
    {!! Form::date('date_order', null, ['class' => 'form-control input-lg', 'placeholder'=>'Data do Pedido', 'id'=>'date_order']) !!}                  
    @endsection

    @section('create_form')     
    {!! Form::open(['url' => ['order/store']]) !!}
    {!! Form::hidden('id', null, ['id'=>'id']) !!}
    {!! Form::label('lsalesman', 'Vendedor') !!}        
    {!! Form::text('salesman', null, ['class' => 'form-control input-lg', 'placeholder'=>'Vendedor', 'id'=>'salesman']) !!}
    {!! Form::label('lclient', 'Cliente') !!} 
    {!! Form::select('client_id', $clients, null, ['class' => 'form-control input-lg', 'id'=>'clients']) !!}
    {!! Form::label('ldate_order', 'Data do Pedido') !!} 
    {!! Form::date('date_order', null, ['class' => 'form-control input-lg', 'placeholder'=>'Data do Pedido', 'id'=>'date_order']) !!}              
    @endsection

    @section('delete_form')
    {!! Form::open(['url' => ['order/destroy']]) !!}
    {!! Form::hidden('id', null, ['id'=>'id']) !!}
    @endsection
      
    @section('js')
    <script>
        $('#EditModal').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget) // Button that triggered the modal            
            
            $(this).find('#salesman').val(button.data('salesman'))
            $(this).find('#clients').val(button.data('client'))
            $(this).find('#date_order').val(button.data('date_order'))
            $(this).find('#id').val(button.data('id'))
        });

        $('#DeleteModal').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget) // Button that triggered the modal            
            
            $(this).find('#id').val(button.data('id'))
        }); 
        
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
              var value = $(this).val().toLowerCase();
              $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
            });
          });        
      </script>
    @endsection()

