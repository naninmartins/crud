<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    private $Product;

    function __construct(Product $product)
    {
        $this->Product = $product;
    }

    public function index()
    {
        $products = $this->Product->all();

        return view('Product/product', compact('products'));
    }

    public function show(Request $request)
    {
        //* Display the specified resource.
       

        if ($request->option == 1)
        {
            $product = $request->only('search');
            $products = $this->Product->where('product_name',$product)->get();

        }
        else
        {
       
            $price = $request->get('search');
            $products = $this->Product->where('price',$price)->get();
    
        }
       
        return view('Product/product', compact('products'));

    }

    public function create(Product $product=null)
    {
        //Show the form for creating a new resource.
        return view('Product/create-edit',compact('product'));
    }

    
    public function store(ProductRequest $request)
    {
     
        $dataform = $request->except(['_token']);

        $insert = $this->Product->create($dataform);


        if ($insert)
           return redirect('product')->with('status-store', 'Produto Cadastrado!');
           
        else
            return redirect('product')->with('erro', 'Erro ao Cadastrar!');
            
      
    }

    public function update(ProductRequest $request)
    {
        //* Update the specified resource in storage.
        //dd($teste=$request->description); se passar o campo com find eu obtenho um array se for direto como objeto obtenho o valor
        
        $product = $this->Product->find($request->id);
        $update = $product->update($request->all());

        if ($update)
           return redirect('product')->with('status-update', 'Produto Atualizado!');
            
        else
            return redirect('product')->with('erro', 'Erro ao Atualizar!');
            
    }
    public function destroy(Request $request)
    {
        //* Remove the specified resource from storage.
        
        $product = $this->Product->find($request->id);
        $delete = $product->delete();

        

        if ($delete)
           return redirect('product')->with('status-destroy', 'Produto Deletado!');

        else
            return redirect('product')->with('erro', 'Erro ao Deletar!');

    }
}
