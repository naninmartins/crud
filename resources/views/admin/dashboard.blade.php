{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('Crud', 'Dashboard')

@section('content_header')
    
@stop

@section('content')

@if (session('status-store'))
<div class="alert alert-success">
    {{ session('status-store') }}
</div>

@elseif (session('status-update'))
<div class="alert alert-info">
    {{ session('status-update') }}
</div>

@elseif (session('status-destroy'))
<div class="alert alert-info">
    {{ session('status-destroy') }}
</div>

@elseif (session('erro'))
<div class="alert alert-error">
    {{ session('status') }}
</div>

@endif


@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); 
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });

    </script>
@stop