<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'salesman' => 'required|min:3|max:100',
            'date_order' => 'required',
        ];

    }

    public function messages()
    {
        return [
            'salesman.required' => 'Nome de preenchimento obrigatório',
            'salesman.min' => 'Nome com min de 3 caracteres e max de 100',
            'date_order.required' => 'Preencher Data de Pedido',
        ];

    }
}
