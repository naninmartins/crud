@extends('admin.dashboard')
@section('content')
@include('admin.message')

<div>     
    <table class="table table-bordered table-hover dataTable" >
        <thead>
            <tr class="form-inline" >
                <div class="col-md-4 pull-right"style="padding-left:0px;">
                    
                    <button  type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#CreateModal" 
                        data-title="Cadastrar Produto">
                        <i class="fa fa-plus"></i>
                    </button>     
                        
                    </div>  
                </div>                     
                    <div class="col-md-3 pull-left" style="padding:0px;">
                    <div class="input-group"> 
                        <input class="form-control" id="myInput" type="text" placeholder="Pesquisar..">                               
                    </div> 
                </div>  
            </tr>             
            <tr role="row">
                <th >
                    Produto
                </th>

                <th >
                    Preço
                </th>

                <th >
                    Descrição
                </th>

                <th >
                    Ações
                </th>     
        
            </tr>
        </thead>

        <tbody id="myTable">
            @foreach($products as $product)
                <tr role="row" class="odd">
                    <td class="sorting_1">{{strtoupper($product->product_name)}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{strtoupper($product->description)}}</td>
                    <td>                                
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#EditModal" 
                        data-title="Editar Produto" 
                        data-id="{{$product->id}}"
                        data-name="{{strtoupper($product->product_name)}}" 
                        data-price="{{$product->price}}" 
                        data-description="{{strtoupper($product->description)}}">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#DeleteModal" 
                        data-id="{{$product->id}}">                                                                    
                        <i class="glyphicon glyphicon-erase"></i>
                    </button>
                    </td>                        
                </tr>
            @endforeach
        </tbody>

        <tfoot>
        </tfoot>
    </table>    
</div>
@stop

@extends('admin.form')

    @section('edit_form')               
        {!! Form::open(['url' => ['product/update'],'method' => 'put']) !!}  
        {!! Form::label('lproduct', 'Nome do Produto') !!}
        {!! Form::hidden('id', null, ['id'=>'id']) !!}                     
        {!! Form::text('product_name', null, ['class' => 'form-control input-lg', 'id'=>'name' , 'placeholder'=>'Produto']) !!}
        {!! Form::label('lprice', 'Preço') !!}
        {!! Form::text('price', null, ['class' => 'form-control input-lg','id'=>'price' , 'placeholder'=>'Preço','maxlength'=>'13']) !!}
        {!! Form::label('ldescription', 'Descrição do Produto') !!}        
        {!! Form::text('description', null, ['class' => 'form-control input-lg','id'=>'description' , 'placeholder'=>'Descrição']) !!}                                    
    @endsection

    @section('create_form')               
        {!! Form::open(['url' => 'product/store']) !!} 
        {!! Form::label('lproduct', 'Nome do Produto') !!}
        {!! Form::hidden('id', null, ['id'=>'id']) !!}                     
        {!! Form::text('product_name', null, ['class' => 'form-control input-lg', 'id'=>'name' , 'placeholder'=>'Produto']) !!}
        {!! Form::label('lprice', 'Preço') !!}
        {!! Form::text('price', null, ['class' => 'form-control input-lg','id'=>'price' , 'placeholder'=>'Preço','maxlength'=>'13']) !!}
        {!! Form::label('ldescription', 'Descrição do Produto') !!}        
        {!! Form::text('description', null, ['class' => 'form-control input-lg','id'=>'description' , 'placeholder'=>'Descrição']) !!}                                         
    @endsection

    @section('delete_form')
    {!! Form::open(['url' => ['product/destroy']]) !!}
    {!! Form::hidden('id', null, ['id'=>'id']) !!}
    @endsection
      
    @section('js')
        <script>
            $('#EditModal').on('show.bs.modal', function (e) {
                var button = $(e.relatedTarget) // Button that triggered the modal            
                
                $(this).find('#name').val(button.data('name'))
                $(this).find('#price').val(button.data('price'))
                $(this).find('#description').val(button.data('description'))
                $(this).find('#id').val(button.data('id'))
            }); 

            $('#DeleteModal').on('show.bs.modal', function (e) {
                var button = $(e.relatedTarget) // Button that triggered the modal            

                $(this).find('#id').val(button.data('id'))
            });
            
            $(document).ready(function(){
                $("#myInput").on("keyup", function() {
                  var value = $(this).val().toLowerCase();
                  $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                  });
                });
              });
        </script>
    @endsection()









