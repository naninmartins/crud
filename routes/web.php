<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
*Routes to Client Group
*/
Route::get('/', 'ClientController@index');

Route::group(['prefix' => 'client'], function () {

Route::get('/index', 'ClientController@index');

Route::post('/show','ClientController@show');

Route::get('/create/{client?}', 'ClientController@create');

Route::post('/store','ClientController@store');

Route::put('/update', 'ClientController@update');

Route::post('/destroy', 'ClientController@destroy');

});

/*
*Routes to Product Group
*/

Route::group(['prefix' => 'product'], function () {

Route::get('/', 'ProductController@index'); 

Route::post('/show','ProductController@show');

Route::get('/create/{product?}', 'ProductController@create');

Route::post('/store','ProductController@store');

Route::put('/update', 'ProductController@update');

Route::post('/destroy', 'ProductController@destroy');

});

/*
*Routes to Order Group
*/

Route::group(['prefix' => 'order'], function () {

    Route::get('/', 'OrderController@index'); 
    
    Route::get('/show/{id}','OrderController@show');

    Route::post('/search','OrderController@search');

    Route::get('/detail/{id}','OrderController@description');
    
    Route::get('/create/{order?}', 'OrderController@create');
    
    Route::post('/store','OrderController@store');
    
    Route::put('/update', 'OrderController@update');
    
    Route::post('/destroy', 'OrderController@destroy');
    
    });

/*
*Routes to Item Group
*/

Route::group(['prefix' => 'item'], function () {

    Route::get('/index', 'ItemController@index'); 
    
    Route::get('/show/{id}','ItemController@show');
    
    Route::get('/create/{item}', 'ItemController@create');
    
    Route::post('/store','ItemController@store');
    
    Route::put('/update', 'ItemController@update');
    
    Route::post('/destroy', 'ItemController@destroy');
    
    });

 





