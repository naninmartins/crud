<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
class Client extends Model
{

    protected $fillable = [
        'name', 'cnpj','date_cad',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $guarded = [
        '_token',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }



    public $rules = [
        'name' => 'required|min:3|max:100',
        'cnpj' => 'required|numeric|min:13|',
    ];


    public $messages = [
        'name.required' => 'Campo de preenchimento obrigatório',
        'name.min' => 'Min de 3 caracteres e max de 100',
        'cnpj.numeric' => 'Digite apenas números',
        'cnpj.required' => 'Campo de preenchimento obrigatório',
        'cnpj.min' => 'Obrigatório 13 caracteres',
    ];
    


}