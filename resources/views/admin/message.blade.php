@if ( isset ($errors) && count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ( $errors->all() as $error)
        <p>
            {{$error}}
        </p>
        @endforeach
    </div>
@endif    

@if (session('status-store'))
<div class="alert alert-success">
    {{ session('status-store') }}
</div>

@elseif (session('status-update'))
<div class="alert alert-info">
    {{ session('status-update') }}
</div>

@elseif (session('status-destroy'))
<div class="alert alert-info">
    {{ session('status-destroy') }}
</div>

@elseif (session('erro'))
<div class="alert alert-error">
    {{ session('status') }}
</div>

@endif
