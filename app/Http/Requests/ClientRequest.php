<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:100',
            'cnpj' => 'required|digits:13',
            'date_cad' => 'required',
        ];

    }

    public function messages()
    {
        return [
            'name.required' => 'Nome de preenchimento obrigatório',
            'name.min' => 'Nome com min de 3 caracteres e max de 100',
            'cnpj.digits' => 'Digite CNPJ válido: nº de 13 dígitos',
            'cnpj.required' => 'CNPJ de preenchimento obrigatório',
            'date_cad.required' => 'Preencher Data de Cadastro',
        ];

    }
}
